# Simple `C` language SDL2 template

The `SDL2_template.h` is a *single file header library* for `C`.

References:

- [https://www.libsdl.org/](https://www.libsdl.org/)
- [https://github.com/nothings/stb](https://github.com/nothings/stb)
- [Single file library howto](https://github.com/nothings/stb/blob/master/docs/stb_howto.txt)

Add this if you only want to use the template:

```c
#include "SDL2_template.h"
```

Add this if you want to use TTF fonts in your app:

```c
#include "SDL2_fonts.h"
```

The example in `main` prints a *black* text on a *silver* background in the
middle of the window.

See the `Makefile` for build details.

The comments indicate where to place code if you have to do something after SDL2
initialization (e.g. TTF font initializations) or just before SDL2 close:

```c
InitGraph();
atexit(CloseGraph);
// Things to do after SDL2 initialization goes here
Loop();
// Things to do before SDL2 closing goes here
```

See the example in `main.c`:

- The *drawing* code goes inside `Draw()` function
- The *keyboard events* are handled in `HandleKeyPress()` function
- The *mouse events* are handled in `HandleMouse()` function

## Usage

- Press `q` to quit

## Build and run

Tested on:

- `Linux` (Debian and Arch)
- `FreeBSD`
- `Windows 10`

### Linux and FreeBSD

```console
make && ./sdl2_app
```

### Windows

Use the `winmake.bat` file.

The app compiles fine with [MinGW](https://sourceforge.net/projects/mingw/)
assuming you set the *path environment variable*:

```
mingw32-make.exe -f WinMakefile
```

Change the following lines from `WinMakefile`  if you installed `SDL2` in
other location:

```
INCLUDE_PATH = C:\MinGW\mingw64\include
LIBS_PATH = C:\MinGW\mingw64\libs
```


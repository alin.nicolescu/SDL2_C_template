#include "SDL2_template.h"
#include "SDL2_fonts.h"

font_t f;

int main(int argc,char* argv[])
{
    (void) argc;
    (void) argv;

    // SDL2 window parameters
    SetWidthHeigth(800,600);
    SetTitle("SDL2 template test");
    SetBackground(&silver);

    InitGraph();
    atexit(CloseGraph);

    // Things to do after SDL2 initialization goes here
    FontInit(&f,"font.ttf",48);
    atexit(CloseFonts);

    Loop();

    // Things to do before SDL2 closing goes here
    FontCleanup(&f);

    return 0;
}

void HandleKeyPress(SDL_Event e)
{
    (void) e;
    /* Use e.key.keysym.sym */
}

void HandleMouse(SDL_Event e)
{
    (void) e;
    /* Use e here */
}

void Draw()
{
    /* Draw here */
    FontSetText(&f,"SDL2 Template",&black);
    FontWrite(&f,(width - f.width)/2,(heigth - f.heigth)/2);
}
